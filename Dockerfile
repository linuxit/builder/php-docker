FROM php:7-fpm
MAINTAINER admin@itk98.net

RUN apt-get update && apt-get install -y curl libcurl4 libcurl4-openssl-dev freetds-dev \
                          zlib1g-dev libzip-dev libonig-dev libpng-dev && \
    ln -s /usr/lib/x86_64-linux-gnu/libsybdb.so /usr/lib/ && \
    docker-php-ext-install -j$(nproc) mysqli pdo pdo_mysql curl opcache pdo_dblib mbstring zip gd

EXPOSE 9000
CMD ["php-fpm"]
